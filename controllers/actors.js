const eventModel = require("../models/event");
const actorModel = require("../models/actor");
const { NOT_FOUND_STATUS, BAD_REQUEST_STATUS } = require("../providers/constants");

const MS_PER_DAY = 1000 * 60 * 60 * 24;

function getActor(eventActorData) {
    return {
        id: eventActorData.actorId,
        login: eventActorData.login,
        avatar_url: eventActorData.avatar_url,
    };
}

function buildUtcDate(date) {
    return new Date(`${date.replace(" ", "T")}Z`);
}

function dateDiffInUtcDays(d1, d2) {
    const date1 = new Date(d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate());
    const date2 = new Date(d2.getUTCFullYear(), d2.getUTCMonth(), d2.getUTCDate());
    const timeDiff = Math.abs(date2.getTime() - date1.getTime());

    return Math.floor(timeDiff / MS_PER_DAY);
}

async function getAllActors() {
    const eventsActorsData = await eventModel.getEventsWithActorsOrderedByTimestamp();

    const actorsMap = new Map();

    for (const data of eventsActorsData) {
        const actorId = data.actorId;

        if (actorsMap.has(actorId)) {
            const actorData = actorsMap.get(actorId);

            ++actorData.events;

            // Not really needed since the object's reference does not change
            // but it improves readability
            actorsMap.set(actorId, actorData);
        } else {
            actorsMap.set(actorId, {
                events: 1,
                actor: getActor(data),
                latestEvent: buildUtcDate(data.created_at),
            });
        }
    }

    const actorsData = [...actorsMap.values()];
    actorsData.sort((ac1, ac2) => {
        if (ac1.events != ac2.events) {
            return ac2.events - ac1.events;
        }

        if (ac1.latestEvent.getTime() != ac2.latestEvent.getTime()) {
            return ac2.latestEvent.getTime() - ac1.latestEvent.getTime();
        }

        return ac1.actor.login.localeCompare(ac2.actor.login);
    });

    return actorsData.map((data) => data.actor);
}

async function getStreak() {
    const eventsActorsData = await eventModel.getEventsWithActorsOrderedByTimestamp();

    const actorsMap = new Map();

    for (const data of eventsActorsData) {
        const actorId = data.actorId;
        const currentEventDate = buildUtcDate(data.created_at);

        if (actorsMap.has(actorId)) {
            const actorData = actorsMap.get(actorId);

            const { lastEventDate } = actorData;

            const dateDiff =
                dateDiffInUtcDays(currentEventDate, lastEventDate);
            if (dateDiff == 1) {
                ++actorData.currentStreak;
                actorData.maxStreak = Math.max(actorData.maxStreak, actorData.currentStreak);
            } else if (dateDiff !== 0) {
                actorData.currentStreak = 1;
            }

            actorData.lastEventDate = currentEventDate;

            // Not really needed since the object's reference does not change
            // but it improves readability
            actorsMap.set(actorId, actorData);
        } else {
            actorsMap.set(actorId, {
                actor: getActor(data),
                lastEventDate: currentEventDate,
                latestEvent: currentEventDate,
                maxStreak: 1,
                currentStreak: 1,
            });
        }
    }

    const actorsData = [...actorsMap.values()];
    actorsData.sort((ac1, ac2) => {
        if (ac1.maxStreak != ac2.maxStreak) {
            return ac2.maxStreak - ac1.maxStreak;
        }

        if (ac1.latestEvent.getTime() != ac2.latestEvent.getTime()) {
            return ac2.latestEvent.getTime() - ac1.latestEvent.getTime();
        }

        return ac1.actor.login.localeCompare(ac2.actor.login);
    });

    return actorsData.map((data) => data.actor);
}

async function updateActor(updatedActor) {
    const actor = await actorModel.getById(updatedActor.id);

    if (!actor) {
        const err = new Error("Actor not found");
        err.status = NOT_FOUND_STATUS;
        throw err;
    }

    if (actor.name != updatedActor.name || actor.login != updatedActor.login) {
        const err = new Error("Changing any field other than the avatar url is disallowed");
        err.status = BAD_REQUEST_STATUS;
        throw err;
    }

    return actorModel.updateAvatarUrl(updatedActor.id, updatedActor.avatar_url);
}

module.exports = {
    updateActor,
    getAllActors,
    getStreak,
};
