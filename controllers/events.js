const eventModel = require("../models/event");
const actorModel = require("../models/actor");
const repoModel = require("../models/repo");
const { NOT_FOUND_STATUS, BAD_REQUEST_STATUS } = require("../providers/constants");

function createMapById(data) {
    return data.reduce((map, element) => {
        map.set(element.id, element);
        return map;
    }, new Map());
}

async function populateEventsActorRepo(events) {
    const actorIds = new Set();
    const repoIds = new Set();

    for (const event of events) {
        actorIds.add(event.actor);
        repoIds.add(event.repo);
    }

    const actors = await actorModel.getByIds([...actorIds]);
    const repos = await repoModel.getByIds([...repoIds]);

    const actorsMap = createMapById(actors);
    const reposMap = createMapById(repos);

    return events.map((ev) => {
        ev.actor = actorsMap.get(ev.actor);
        ev.repo = reposMap.get(ev.repo);
        return ev;
    });
}

async function getAllEvents() {
    const events = await eventModel.getAll();
    return populateEventsActorRepo(events);
}

async function addEvent(newEvent) {
    const event = await eventModel.getById(newEvent.id);

    if (event) {
        const err = new Error("Event with same id exists");
        err.status = BAD_REQUEST_STATUS;
        throw err;
    } else {
        return Promise.all([actorModel.insertIfNeeded(newEvent.actor),
            repoModel.insertIfNeeded(newEvent.repo), eventModel.insert(newEvent)]);
    }
}

async function getByActor(actorId) {
    const normalizedActorId = Number(actorId);
    const actor = await actorModel.getById(normalizedActorId);

    if (actor) {
        const events = await eventModel.getByActorId(normalizedActorId);
        return populateEventsActorRepo(events);
    } else {
        const err = new Error("Actor not found");
        err.status = NOT_FOUND_STATUS;
        throw err;
    }
}

function eraseEvents() {
    return Promise.all([eventModel.deleteAll(), actorModel.deleteAll(), repoModel.deleteAll()]);
}

module.exports = {
    getAllEvents,
    addEvent,
    getByActor,
    eraseEvents,
};
