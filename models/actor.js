const db = require("../providers/db");
const { ACTOR_TABLE_NAME: TABLE_NAME } = require("../providers/constants");

function createTable() {
    const command = `CREATE TABLE ${TABLE_NAME}` +
        "(id INT(11) NOT NULL, login TEXT, avatar_url TEXT, " +
        "UNIQUE(id), UNIQUE(login))";
    return db.run(command);
}

async function insertIfNeeded(newActor) {
    const actor = await getById(newActor.id);

    if (!actor) {
        const command = `INSERT INTO ${TABLE_NAME} (id, login, avatar_url) ` +
            `VALUES(${newActor.id}, '${newActor.login}', '${newActor.avatar_url}')`;
        return db.run(command);
    }
}

function getById(id) {
    return db.get(`SELECT * FROM ${TABLE_NAME} WHERE id = ${id}`);
}

function getByIds(ids) {
    return db.all(`SELECT * FROM ${TABLE_NAME} WHERE id IN (${ids.join(",")})`);
}

function updateAvatarUrl(id, newUrl) {
    return db.run(`UPDATE ${TABLE_NAME} SET avatar_url = '${newUrl}' WHERE id = ${id}`);
}

function deleteAll() {
    return db.run(`DELETE FROM ${TABLE_NAME}`);
}

module.exports = {
    createTable,
    insertIfNeeded,
    getById,
    getByIds,
    updateAvatarUrl,
    deleteAll,
};
