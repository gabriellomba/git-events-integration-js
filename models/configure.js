const modelFiles = ["./actor.js", "./event.js", "./repo.js"];

function configure() {
    return Promise.all(modelFiles.map((file) => {
        const model = require(file);

        return model.createTable();
    }));
}

module.exports = configure;
