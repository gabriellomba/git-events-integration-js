const db = require("../providers/db");
const { EVENT_TABLE_NAME: TABLE_NAME, ACTOR_TABLE_NAME } = require("../providers/constants");

function createTable() {
    const command = `CREATE TABLE ${TABLE_NAME}` +
        "(id INT(11) NOT NULL, type TEXT, actor INT(11), " +
        "repo INT(11), created_at TIMESTAMP, UNIQUE(id))";
    return db.run(command);
}

function insert(newEvent) {
    const command = `INSERT INTO ${TABLE_NAME} (id, type, actor, repo, created_at) ` +
        `VALUES(${newEvent.id}, '${newEvent.type}', ${newEvent.actor.id}, ` +
        `${newEvent.repo.id}, '${newEvent.created_at}')`;
    return db.run(command);
}

function getAll() {
    return db.all(`SELECT * FROM ${TABLE_NAME} ORDER BY id`);
}

function getById(id) {
    return db.get(`SELECT * FROM ${TABLE_NAME} WHERE id = ${id}`);
}

function getByActorId(actorId) {
    return db.all(`SELECT * FROM ${TABLE_NAME} WHERE actor = ${actorId} ORDER BY id`);
}

function deleteAll() {
    return db.run(`DELETE FROM ${TABLE_NAME}`);
}

function getEventsWithActorsOrderedByTimestamp() {
    return db.all(`SELECT AC.id as actorId, * FROM ${TABLE_NAME} EV\
        INNER JOIN ${ACTOR_TABLE_NAME} AC on AC.id = EV.actor ORDER BY EV.created_at DESC`);
}

module.exports = {
    createTable,
    insert,
    getAll,
    getById,
    getByActorId,
    deleteAll,
    getEventsWithActorsOrderedByTimestamp,
};
