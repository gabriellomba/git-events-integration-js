const db = require("../providers/db");
const { REPO_TABLE_NAME: TABLE_NAME } = require("../providers/constants");

function createTable() {
    return db.run(`CREATE TABLE ${TABLE_NAME}` +
        "(id INT(11) NOT NULL, name TEXT, url TEXT, UNIQUE(id))");
}

async function insertIfNeeded(newRepo) {
    const repo = await getById(newRepo.id);

    if (!repo) {
        return db.run(`INSERT INTO ${TABLE_NAME} (id, name, url) ` +
            `VALUES(${newRepo.id}, '${newRepo.name}', '${newRepo.url}')`);
    }
}

function getById(id) {
    return db.get(`SELECT * FROM ${TABLE_NAME} WHERE id = ${id}`);
}

function getByIds(ids) {
    return db.all(`SELECT * FROM ${TABLE_NAME} WHERE id IN (${ids.join(",")})`);
}

function deleteAll() {
    return db.run(`DELETE FROM ${TABLE_NAME}`);
}

module.exports = {
    createTable,
    insertIfNeeded,
    getById,
    getByIds,
    deleteAll,
};
