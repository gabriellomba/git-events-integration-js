module.exports = {
    ACTOR_TABLE_NAME: "Actor",
    EVENT_TABLE_NAME: "Event",
    REPO_TABLE_NAME: "Repo",

    NOT_FOUND_STATUS: 404,
    BAD_REQUEST_STATUS: 400,
    CREATED_STATUS: 201,
};
