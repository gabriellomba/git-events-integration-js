const util = require("util");

const sqlite3 = require("sqlite3").verbose();

const db = new sqlite3.Database(":memory:");

// Promisify methods for better integration with async / await
db.run = util.promisify(db.run);
db.get = util.promisify(db.get);
db.all = util.promisify(db.all);

// Export db object in a separate module such that
// there will be only one db instance at runtime
module.exports = db;
