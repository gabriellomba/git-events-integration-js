const express = require("express");

const actorController = require("../controllers/actors");

// eslint-disable-next-line new-cap
const router = express.Router();

router.get("/", async function(req, res, next) {
    try {
        const actors = await actorController.getAllActors();

        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(actors));
    } catch (err) {
        next(err);
    }
});

router.get("/streak", async function(req, res, next) {
    try {
        const actors = await actorController.getStreak();

        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(actors));
    } catch (err) {
        next(err);
    }
});

router.put("/", async function(req, res, next) {
    try {
        const actor = req.body;
        await actorController.updateActor(actor);

        res.end();
    } catch (err) {
        next(err);
    }
});

module.exports = router;
