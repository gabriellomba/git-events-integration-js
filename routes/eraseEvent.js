const express = require("express");

const eventController = require("../controllers/events");

// eslint-disable-next-line new-cap
const router = express.Router();

router.delete("/", async function(req, res, next) {
    try {
        await eventController.eraseEvents();

        res.end();
    } catch (err) {
        next(err);
    }
});

module.exports = router;
