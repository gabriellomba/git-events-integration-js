const express = require("express");

const eventController = require("../controllers/events");
const { CREATED_STATUS } = require("../providers/constants");

// eslint-disable-next-line new-cap
const router = express.Router();

router.post("/", async function(req, res, next) {
    try {
        const newEvent = req.body;
        await eventController.addEvent(newEvent);

        res.writeHead(CREATED_STATUS);
        res.end();
    } catch (err) {
        next(err);
    }
});

router.get("/", async function(req, res, next) {
    try {
        const events = await eventController.getAllEvents();

        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(events));
    } catch (err) {
        next(err);
    }
});

router.get("/actors/:actorId", async function(req, res, next) {
    try {
        const actorId = req.params.actorId;
        const events = await eventController.getByActor(actorId);

        res.setHeader("Content-Type", "application/json");
        res.send(JSON.stringify(events));
    } catch (err) {
        console.log("ERRO GET ACTOR", err);
        next(err);
    }
});

module.exports = router;
